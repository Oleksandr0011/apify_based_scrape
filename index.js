const fs = require('fs');
const https = require('https');
const schedule = require('node-schedule');

const deUrl = "https://dievolkswirtschaft.ch/de/feed/";
const frUrl = "https://dievolkswirtschaft.ch/fr/feed/";

console.log(deUrl.split("/")[3])

function scrape(url) {
    const currentDatetime = new Date();
    const fileName = `${url.split("/")[3]}/${currentDatetime.toISOString().replace(/:/g, "-")}.xml`;

    https.get(url, (res) => {
        let data = '';
        res.on('data', (chunk) => {
            data += chunk;
        });

        res.on('end', () => {
            fs.writeFileSync(`data/${fileName}`, data, 'utf-8');
        });
    });
}

function job() {
    [deUrl, frUrl].forEach(url => {
        scrape(url);
    });
}

// schedule.scheduleJob('0 10 * * *', job);
schedule.scheduleJob('*/10 * * * * *', job);
